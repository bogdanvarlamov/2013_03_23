-- this reservation starts before the start date of our vacation
-- and ends after the end date; therefore the room is unavailable.
-- but both the initial answer query and the extended answer
-- incorrectly returns room 4 as available
INSERT INTO Reservations(RoomNum, Start, [End])
VALUES        (4, '2013-02-10', '2013-02-20')