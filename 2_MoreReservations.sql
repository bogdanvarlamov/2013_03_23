-- this will create a new reservation for room 2
-- it will not conflict with the test reservation from the first query
-- but will cause the room number 2 to be returned
-- even though there is another reservation for room 2 which DOES conflict
-- (so room 2 should not be returned in the list of available rooms)
INSERT INTO Reservations(RoomNum, Start, [End])
VALUES        (2, '2013-06-01', '2013-06-05')