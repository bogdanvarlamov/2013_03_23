--can't declare variables in sql ce
-- so i've got the dates hardcoded in there for this example


--we want all the room numbers that aren't in the conflict set
SELECT DISTINCT	RoomNum
FROM 	Reservations
WHERE	RoomNum NOT IN
(--this get's all of the conflicting reservations
	SELECT        RoomNum
	FROM          Reservations AS innerRS
	WHERE        (Start BETWEEN '2013-02-14' AND  '2013-02-17')
			  OR ([End] BETWEEN '2013-02-14' AND '2013-02-17')
)