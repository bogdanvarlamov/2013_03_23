-- this query is just like the extended answer
-- except it also counts date ranges that span the target reservation date
-- as being in the conflict set

SELECT DISTINCT	RoomNum
FROM 	Reservations
WHERE	RoomNum NOT IN
(--this get's all of the conflicting reservations
	SELECT        RoomNum
	FROM          Reservations AS innerRS
	WHERE        (Start BETWEEN '2013-02-14' AND  '2013-02-17') -- reservations can't start in our target date range
			  OR ([End] BETWEEN '2013-02-14' AND '2013-02-17') -- reservations can't end in our target date range
			  OR (Start < '2013-02-14') AND ([End] > '2013-02-17') -- reservations can't span our target date range
)