-- this query checks which rooms are available
-- for a vacation starting 2013-02-14 and ending 2013-02-17

-- this date range conflicts with the reservation made for room 2
-- and so only rooms 1,3,4 are returned

-- however, this query only works if nobody ever reserves the same room 
-- more than once 
SELECT DISTINCT        RoomNum
FROM            Reservations
WHERE        ([End] < '2013-02-14') OR
                         (Start > '2013-02-17')